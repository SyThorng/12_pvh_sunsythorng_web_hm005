import React, { useState } from "react";
import BtnInputFrom from "./BtnInputFrom";

const Center = ({ data, setData }) => {
    const beach =
        "w-[120px] text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 ";
    const mountain =
        "text-white bg-gradient-to-r from-green-400 via-green-500 to-green-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-green-300 dark:focus:ring-green-800 shadow-lg shadow-green-500/50 dark:shadow-lg dark:shadow-green-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2";
    const forest =
        "w-[100px]  text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 shadow-lg shadow-cyan-500/50 dark:shadow-lg dark:shadow-cyan-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2";

    const [btndata, setBtnData] = useState([]);

    const changeButton = (id) => {
        setData((prevdata) =>
            prevdata.map((item) => {
                if (item.id === id) {
                    if (item.status === "beach") {
                        return { ...item, status: "mountain" };
                    } else if (item.status === "mountain") {
                        return { ...item, status: "forest" };
                    } else {
                        return { ...item, status: "beach" };
                    }
                }
                return item;
            })
        );
    };
    return (
        <div>
            <div className="h-screen  py-10">
                <div className="flex justify-between">
                    <h1
                        class="font-medium text-transparent text-4xl bg-clip-text bg-gradient-to-r from-sky-400 to-pink-50"
                    >
                        Good Evening Team!
                    </h1>

                    <BtnInputFrom datas={data} setData={setData} />
                </div>
                <div className="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-6 mt-4">
                    {data.map((e) => (
                        <div className="w-72 rounded-xl p-5 flex flex-col gap-3 text-white bg-slate-700">
                            <h1 className="text-xl font-medium uppercase">{e.title}</h1>
                            <p className="line-clamp-3 ">{e.description}</p>
                            <div>
                                <p className="text-gray-300 text-sm">People Going</p>
                                <h3 className="text-xl font-medium">{e.peopleGoing}</h3>
                            </div>
                            <div className="flex justify-between items-center gap-4">
                                <button
                                    onClick={() => changeButton(e.id)}
                                    type="button"
                                    className={
                                        e.status === "beach"
                                            ? beach
                                            : e.status === "mountain"
                                                ? mountain
                                                : forest
                                    }
                                >
                                    {e.status}
                                </button>
                                <div>
                                    <label
                                        onClick={() => setBtnData(e)}
                                        htmlFor="model"
                                        className="btn w-32 text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5  text-center "
                                    >
                                        SHOW DETAILS
                                    </label>
                                    {/* {console.log(e.id)} */}
                                    <input type="checkbox" id="model" className="modal-toggle" />
                                    <div className="modal">
                                        <div className="modal-box relative bg-white">
                                            <label
                                                htmlFor="model"
                                                className="btn btn-sm btn-circle absolute right-2 top-2"
                                            >
                                                X
                                            </label>
                                            <h3 className="text-lg font-bold text-black title">
                                                {btndata.title}❤️
                                            </h3>
                                            <p className="py-4 text-black description">
                                                {btndata.description}
                                            </p>
                                            <p className="py-4 text-black font-bold peopleGoing">
                                                Around {btndata.peopleGoing} poeple going there
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Center;
