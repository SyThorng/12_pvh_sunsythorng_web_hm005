

import React from 'react'

const Sidebarr = () => {
    return (
        <div>
            <div class="sidebar fixed top-0 bottom-0 w-24 overflow-y-auto text-center bg-sky-600 ">
                <div className="p-3">
                    <div className="pt-2">
                        <ul>
                            <li>
                                <img
                                    src={require("../assets/Images/category_icon.png")}
                                    className=" w-9 m-auto "
                                />
                            </li>
                        </ul>
                    </div>
                    <ul>
                        <li>
                            <img
                                src={require("../assets/Images/users.png")}
                                className="w-9 m-auto mt-7 "
                            />
                        </li>
                        <li>
                            <img
                                src={require("../assets/Images/success.png")}
                                className="w-9 m-auto pt-2"
                            />
                        </li>
                        <li>
                            <img
                                src={require("../assets/Images/list.png")}
                                className="w-9 m-auto pt-2"
                            />
                        </li>
                        <li>
                            <img
                                src={require("../assets/Images/messenger.png")}
                                className="w-9 m-auto pt-2"
                            />
                        </li>
                    </ul>
                </div>

                <div className="pt-10">
                    <ul>
                        <li>
                            <img
                                src={require("../assets/Images/cube.png")}
                                className="w-9 m-auto "
                            />
                        </li>
                        <li>
                            <img
                                src={require("../assets/Images/comment.png")}
                                className="w-9 m-auto pt-2"
                            />
                        </li>
                        <li>
                            <img
                                src={require("../assets/Images/security.png")}
                                className="w-9 m-auto pt-2"
                            />
                        </li>
                        <li>
                            <img
                                src={require("../assets/Images/notification.png")}
                                className="w-9 m-auto pt-2"
                            />
                        </li>
                    </ul>
                </div>
                <div className="pt-12">
                    <ul>
                        <li>
                            <img
                                src={require("../assets/Images/nonamesontheway.jpg")}
                                className="w-10 m-auto h-10 rounded-full"
                            />
                        </li>
                        <li>
                            <img
                                src={require("../assets/Images/lachlan.jpg")}
                                className="mt-4 w-10 m-auto h-10 rounded-full"
                            />
                        </li>
                        <li>
                            <img
                                src={require("../assets/Images/raamin.jpg")}
                                className="mt-4 w-10 m-auto h-10 rounded-full"
                            />
                        </li>
                        <li>
                            <img
                                src={require("../assets/Images/plus.png")}
                                className="mt-4 w-9 m-auto mt-6"
                            />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Sidebarr