import React from "react";

const Right = () => {
    return (
        <div
            className="
   bg-no-repeat bg-cover bg-center
    bg-[url('https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/d726af82633527.5d23b268e26ba.gif')]
    h-screen fixed top-0 right-0 w-80
     "
        >
            <div className="flex ml-24 w-56  h-20 mt-5">
                <img src={require("../assets/Images/notification.png")} alt=""
                    className="w-16 h-10 px-3" />
                <img src={require("../assets/Images/comment.png")} alt=""
                    className="w-10 h-10 ml-3" />
                <img src={require("../assets/Images/nonamesontheway.jpg")} alt=""
                    className="ml-5 w-10 h-10 rounded-full" />
            </div>
            <div>
                <button type="button" class="ml-36 text-gray-900 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 mr-2 mb-2">
                    My amazing trip
                </button>
            </div>
            <div className="w-[250px] font-extrabold ml-8 mt-8 text-white">
                I like laving down on the sand and looking at the moon
            </div>
            <div className="mt-16">
                <h3 className="px-4 text-lg text-white">77 people going to this trip</h3>
                <div className="py-2 px-4 flex justify-start gap-4">
                    <img
                        className=" rounded-full w-[40px] h-[42px]"
                        src={require("../assets/Images/raamin.jpg")}
                        alt=""
                    />
                    <img
                        className=" rounded-full w-[40px] h-[42px]"
                        src={require("../assets/Images/lachlan.jpg")}
                        alt=""
                    />
                    <img
                        className=" rounded-full w-[40px] h-[42px]"
                        src={require("../assets/Images/nonamesontheway.jpg")}
                        alt=""
                    />
                    <img
                        className=" rounded-full w-[40px] h-[42px] pointer"
                        src={require("../assets/Images/christina.jpg")}
                        alt=""
                    />
                    <p className=" rounded-full w-[40px] h-[42px] bg-sky-500 text-white text-center p-2 font-extrabold">12+</p>
                </div>
            </div>
        </div>
    );
};

export default Right;
