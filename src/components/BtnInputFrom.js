import React, { useState } from "react";

const BtnInputFrom = ({ datas, setData }) => {
    const [newdata, setNewData] = useState([]);

    const onChange = (e) => {
        setNewData({ ...newdata, [e.target.name]: e.target.value });
    };
    const submitData = (e) => {
        e.preventDefault();
        setData([...datas, { id: datas.length + 1, ...newdata }]);
    };
    return (
        <div>
            <div>
                {/* The button to open modal */}
                <label htmlFor="my-modal-2" className="btn">
                    open modal
                </label>
                {/* Put this part before </body> tag */}
                <input type="checkbox" id="my-modal-2" className="modal-toggle" />
                <div className="modal">
                    <div className="modal-box relative bg-white">
                        <label
                            htmlFor="my-modal-2"
                            className="btn btn-sm btn-circle absolute right-2 top-2"
                        >
                            ✕
                        </label>
                        <form onSubmit={submitData}>
                            <div class="mb-6">
                                <label
                                    for="email"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Title
                                </label>
                                <input
                                    type="text"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                    placeholder="Title"
                                    name="title"
                                    onChange={onChange}
                                />
                            </div>
                            <div class="mb-6">
                                <label
                                    for="email"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Description
                                </label>
                                <input
                                    type="text"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                    placeholder="Description"
                                    name="description"
                                    onChange={onChange}
                                />
                            </div>
                            <div class="mb-6">
                                <label
                                    for="email"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    People going
                                </label>
                                <input
                                    type="text"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                    placeholder="Number of people going"
                                    name="peopleGoing"
                                    onChange={onChange}
                                />
                            </div>
                            <div className="mb-6">
                                <label
                                    for="countries"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Type of advanture
                                </label>
                                <select
                                    id="countries"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    name="status"
                                    onChange={onChange}
                                >
                                    <option value="beach">Choose any option</option>
                                    <option value="beach">Beach</option>
                                    <option value="mountain">Mountain</option>
                                    <option value="forest">Forest</option>
                                </select>
                            </div>

                            <button
                                type="submit"
                                class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 btn"
                            >
                                Submit
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BtnInputFrom;
